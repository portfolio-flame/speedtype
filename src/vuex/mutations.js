import text from '../assets/text.json'
import _ from 'lodash'

export default {
  register (state, user) {
    state.user.name = user
    state.user.score = []
  },

  generate (state) {
    if (state.round >= state.totalRounds) {
      state.round = 0
    }

    state.course = _.map(_.shuffle(text[state.round]), (row, index) => {
      return {index, text: row, done: false, duration: 0}
    })
    state.totalRounds = text.length
  },

  update (state, payload) {
    // update item
    let item = _.find(state.course, row => {
      return row.index === payload.index
    })

    if (!item) {
      return
    }

    item.done = payload.done
    item.duration = payload.duration
  },

  saveRound (state, payload) {
    // update user score
    let data = {
      round: payload.round,
      items: payload.items,
      time: payload.time,
      hour: payload.hour,
      minute: payload.minute,
      second: payload.second,
      mistakes: payload.mistakes,
      mistakePercent: payload.mistakePercent,
      wordsPerMinute: payload.wordsPerMinute
    }

    state.user.score.push(data)
  },

  clearRounds (state) {
    state.user.score = []
    state.round = 0
  }
}

export function formatTime (n) {
  return (parseInt(n, 10) >= 10 ? '' : '0') + n
}

/**
 * @used-by item-level timer, round-level timer
 * @param timer
 * @returns {*}
 */
export function adjustTick (timer) {
  if (timer.sec === 60) {
    timer.sec = 0
    timer.min++
  }
  if (timer.min === 60) {
    timer.min = 0
    timer.hr++
  }
  return timer
}
