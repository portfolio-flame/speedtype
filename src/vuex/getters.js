import _ from 'lodash'

export default {
  getUser (state) {
    return state.user.name
  },

  getProgress (state) {
    let done = _.filter(state.course, row => {
      return row.done
    }).length

    return Math.round((done / state.course.length) * 100)
  },

  getItems (state) {
    return state.course
  },

  getNextItem (state) {
    let nextItem = _.filter(state.course, row => {
      return row.done
    }).length
    return nextItem
  },

  getNextRound (state) {
    let itemsDone = _.filter(state.course, row => {
      return row.done
    }).length

    if (itemsDone === state.course.length) {
      state.round++
    }

    return state.round
  },

  getTotalRounds (state) {
    return state.totalRounds
  },

  getRoundStats (state) {
    return state.user.score
  },

  getGameStats (state) {
    let stats = {
      mistakes: 0,
      mistakePercent: 0,
      wordsPerMinute: 0
    }
    let mistakePercent = 0
    let wpm = 0
    let rounds = state.user.score

    if (rounds.length > 0) {
      for (let x = 0; x < rounds.length; x++) {
        stats.mistakes += rounds[x].mistakes
        mistakePercent += rounds[x].mistakePercent
        wpm += rounds[x].wordsPerMinute
      }
      stats.mistakePercent = parseInt(Math.round(mistakePercent / rounds.length))
      stats.wordsPerMinute = parseInt(Math.round(wpm / rounds.length))
    }

    return stats
  }
}
