import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations.js'
import getters from './getters'

Vue.use(Vuex)

const state = {
  user: {
    name: '',
    score: []
  },
  course: [], // the list of items in one round
  round: 0, // the current round
  totalRounds: 0
}

export default new Vuex.Store({
  state,
  mutations,
  getters
})
